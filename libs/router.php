<?php

class Router
{
    private $request;
    private $params;
    private $controler;
    private $control;
    
    function __construct()
    {
        $this->request = $_GET['url'];
        $this->params = explode("/", rtrim($this->request, "/"));
        $this->controler = $this->params[0];
        if($this->controler == "index.php")
        {
            $this->controler = "Index";
        }
        
        $this->controler = ucfirst($this->controler);
        
        $file = 'controllers/'.$this->controler.".php";
        if(file_exists($file))
        {
            require_once $file;
            $this->control = new $this->controler($this->params);
        }
        
        else
            echo 'Nie ma takiej metody';
        
        //echo $this->controler;
        //echo $this->controler;
    }
}