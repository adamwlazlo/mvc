<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class View
{
    function __construct()
    {
    }
    
    public function render()
    {
        require_once 'views/header.php';
        $plik = '';
        if(isset($this->controller) && isset($this->page))
            $plik = "views/".$this->controller."/".$this->page.".php";
        if(file_exists($plik))
            require_once $plik;
        else
        {
            $this->message = "Nie znleziono pliku";
            require_once 'views/error.php';
        }
        //echo "<br />".$this->controller;
        //require_once 'view/'.$this->controller;
        
        require_once 'views/footer.php';
    }
}