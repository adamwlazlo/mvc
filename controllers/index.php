<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Index extends controller
{
    private $model;
    private $date;
    private $time;
    private $location;
    
    function __construct($params)
    {
        parent::__construct();
        $this->view->controller = "Index";
        $this->view->page = "news";
        
        require_once 'models/IndexModel.php';
        $this->model = new Index_model();

        
        $action = "News";
        if(isset($params[1])) $action = ucfirst($params[1]);
        
        $this->date = "Today";
        if(isset($params[2])) $this->date = ucfirst($params[2]);
        
        $this->time = "AllDay";
        if(isset($params[3])) $this->time = ucfirst($params[3]);
        
        $this->location = "Poland";
        if(isset($params[4])) $this->location = ucfirst($params[4]);

        $this->$action($this->date, $this->time, $this->location);
    }
    
    private function News($date, $time, $location)
    {
        $this->view->title = "News!!! ". $date." - ".$time."<br/> From: ".$location."<br />";
        $this->view->mult = $this->model->Calc(13, 4)."<br />";
        $this->view->author = "AW";
        $this->view->render();
    }
    
    private function Photos()
    {
        echo "News!!! ". $date." - ".$time."<br/> From: ".$location;
    }
}

